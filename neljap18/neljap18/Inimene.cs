﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neljap18
{
    class Inimene
    {
        // static member - kõigile inimestele ühine
        public static List<Inimene> Inimesed = new List<Inimene>();

        // inimese väljad
        private string _EesNimi;
        private string _PereNimi;
        private int _Vanus;

        public string EesNimi
        {
            get => _EesNimi;
            set
            {
                if (value.Length > 1)
                    _EesNimi = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
                else _EesNimi = value.ToUpper();
            }
        }
        public string PereNimi
        {
            get => _PereNimi;
            set
            {
                if (value.Length > 1)
                    _PereNimi = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
                else _PereNimi = value.ToUpper();
            }
        }


        public int getVanus()
        {
            return _Vanus;
        }

        public void setVanus(int uusVanus)
        {
            if (uusVanus > _Vanus && uusVanus < 120) _Vanus = uusVanus;
            else Console.WriteLine($"liiga {(uusVanus < 0 ? "väike" : "suur")} vanus {uusVanus}");
        }

        public int Vanus // property    
                         // eksponeerib private välja _Vanus
        {
            get { return _Vanus; }    // getter - funktsioon
            set                       // setter - meetod
            {
                // väli nimega value
                if (value > _Vanus && value < 120) _Vanus = value;
                else Console.WriteLine($"liiga {(value < 0 ? "väike" : "suur")} vanus {value}");

            }
        }

        // parameetroiteta konstruktor
        public Inimene()
        {
            Inimesed.Add(this);
        }

        // parameetritega konstruktor
        public Inimene(string eesNimi, string pereNimi)
        {
            this.EesNimi = eesNimi;
            PereNimi = pereNimi;
            Inimesed.Add(this);
        }

        public void EemaldaNimekirjast()
        {
            Inimesed.Remove(this);
        }



        // lühike formaat funktsioonist
        public string TäisNimi() => this.EesNimi + " " + this.PereNimi;

        // näide overloadimisest
        public string TäisNimi(string eraldaja)
        {
            return EesNimi + eraldaja + PereNimi;
        }

        // pikem formaat samast funktsioonist
        public string FullName()
        {
            return $"{this.EesNimi} {this.PereNimi}";
        }
        // ToString funktsioon
        public override string ToString() => $"Inimene {FullName()} vanusega {_Vanus}";

    }

}

