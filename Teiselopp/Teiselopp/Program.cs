﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Teiselopp
{

    static class Program
    {
        public static TimeSpan Teisenda(this string x)
        {
            string.Join(":",
            ("0:0:" + x)   // lisab ette mõttetud "0:"-d
                            .Split(':')   // teeb stringi massiiviks
                            .Reverse()    // pöörab massiivi tagurpidi
                            .Take(3)      // võtab kolm esimest (ehk siis viimast)
                            .Reverse()   // pöörab uuesti tagurpidi
                        );

        
        

        string filename = @"..\..\spordipäeva protokoll.txt";


        var filesisu = File.ReadAllLines(filename)
            .Skip(1) // päiserida vahele
            .Select(x => x.Trim()) // ülearused tühikud minema
            .Where(x => Length > 0) //tühjad read välja
            .Select(x => x.Split(','))
            .Select(x => new { Nimi = x[0].Trim(), Distants = x[1].Trim(), Aeg = x[2].Trim(), Teisenda() })
            .ToArray() //et jääks meelde ja ei peak
            ;
        foreach (var x in filesisu) Console.WriteLine(x);

        }
    
    }
        
}

    


