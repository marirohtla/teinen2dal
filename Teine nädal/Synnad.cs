﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Teine_nädal
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Nimekiri.txt"; //faili nimi
            string[] loetudRead = File.ReadAllLines(filename); //MASSIIV
           
            // (var x in loetudRead) Console.WriteLine 8(x);
            Dictionary<string, DateTime?> nimekiri = new Dictionary<string, DateTime?>();
            foreach (var loetudRida in loetudRead)
            {
                string[] osad = loetudRida.Split(',');
                string nimi = osad[0];
                DateTime? sünniaeg = null;
                if (osad.Length > 1)
                {
                    DateTime.TryParse(osad[1], out DateTime sa);
                    if (sa.Year > 1900 && sa <DateTime.Now) sünniaeg = sa;
                }
                nimekiri.Add(nimi, sünniaeg);
            }
            foreach (var x in nimekiri.Keys)
            {
                Console.WriteLine($"Tegelase {x} sünnipäev on {nimekiri[x]}");
            }
            //DateTime v = DateTime.Now;
            //foreach (var x in nimekiri.Values)
            //    if (x.HasValue && x < v) v = x.Value;
            //        Console.WriteLine($"Vanim sünnipäev on {v}"); //et leida kõige vanem inimene
            //Console.WriteLine($"ka niimoodi {nimekiri.Values.Min()}");

            DateTime v = DateTime.Now.AddYears(1);
            string kellel = "";
            foreach(var x in nimekiri)
            {
                if (x.Value.HasValue)
                {
                    var d = x.Value.Value; //.value on sünnipäev
                    int m = DateTime.Now.Year - d.Year;
                    d = d.AddYears(m);
                    if (d < DateTime.Now) d=d.AddYears(1); // kas on tänasest ees või taga pool, vastavalt liidab juurde
                    if (d < v) { v = d; kellel = x.Key; } // .Key on nimi
                }
            }
            Console.WriteLine($"järgmine sünnipäevalaps on {kellel}, kellel on sünnipäev {v.ToShortDateString()}"); //aga kellel??



        }
    }
}
